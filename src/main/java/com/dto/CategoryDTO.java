package com.dto;

import com.entity.User;

import javax.persistence.*;

/**
 * Created by Onur Kuru on 11.7.2015.
 */

public class CategoryDTO {


    private int id;

    private String name;

    private UserDTO user;

    public CategoryDTO() {
        user = new UserDTO();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
