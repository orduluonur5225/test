package com.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Onur Kuru on 11.7.2015.
 */


public class CountDownDTO {

    private int countDownID;

    private String countDownName;

    private List<UserDTO> userList = new ArrayList<UserDTO>();

    public int getCountDownID() {
        return countDownID;
    }

    public void setCountDownID(int countDownID) {
        this.countDownID = countDownID;
    }

    public String getCountDownName() {
        return countDownName;
    }

    public void setCountDownName(String countDownName) {
        this.countDownName = countDownName;
    }

    public List<UserDTO> getUserList() {
        return userList;
    }

    public void setUserList(List<UserDTO> userList) {
        this.userList = userList;
    }
}
