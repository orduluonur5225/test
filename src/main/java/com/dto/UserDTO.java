package com.dto;

import com.entity.Category;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Onur Kuru on 11.7.2015.
 */


public class UserDTO {

    private int userID;

    private String userName;

    private List<CountDownDTO> countDownList = new ArrayList<CountDownDTO>();

    private List<CategoryDTO> categoryDTOList = new ArrayList<CategoryDTO>();


    public int getUserID() {
        return userID;
    }

    public void setCategoryDTOList(List<CategoryDTO> categoryDTOList) {
        this.categoryDTOList = categoryDTOList;
    }


    public List<CategoryDTO> getCategoryDTOList() {
        return categoryDTOList;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<CountDownDTO> getCountDownList() {
        return countDownList;
    }

    public void setCountDownList(List<CountDownDTO> countDownList) {
        this.countDownList = countDownList;
    }
}
