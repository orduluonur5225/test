package com.dao;

import com.dto.CategoryDTO;
import com.dto.CountDownDTO;
import com.dto.UserDTO;
import com.entity.Category;
import com.entity.CountDown;
import com.entity.User;
import net.sf.ehcache.search.aggregator.Count;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Onur Kuru on 11.7.2015.
 *
 */
public class test {

    public static UserDTO toDTO (User user) {

        UserDTO userDTO = new UserDTO();

        userDTO.setUserID(user.getUserID());
        userDTO.setUserName(user.getUserName());
        for(CountDown countDown : user.getCountDownList()){
            userDTO.getCountDownList().add(toCountDownDTO(countDown));
        }

        for(Category category : user.getCategoryList()){
            userDTO.getCategoryDTOList().add(categoryDTO(category));
        }

        return userDTO;
    }

    public static CountDownDTO toCountDownDTO (CountDown countDown) {

        CountDownDTO countDownDTO = new CountDownDTO();

        countDownDTO.setCountDownID(countDown.getCountDownID());
        countDownDTO.setCountDownName(countDown.getCountDownName());
        for(User user : countDown.getUserList()){
            countDownDTO.getUserList().add(toDTO(user));
        }

        return countDownDTO;

    }

    public static CategoryDTO categoryDTO (Category category) {

        CategoryDTO categoryDTO = new CategoryDTO();

        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setUser(toDTO(category.getUser()));

        return categoryDTO;
    }


    public static void main(String args[]){

        User u2 = new User();
        u2.setUserName("u2");
        u2.setUserID(2);


        CountDown c1 = new CountDown();
        CountDown c2 = new CountDown();
        c1.getUserList().add(u2);
        c2.getUserList().add(u2);

        Category category1 = new Category();
        Category category2 = new Category();

        category1.setUser(u2);
        category2.setUser(u2);

        User u1 = new User();

        u1.setUserID(1);
        u1.setUserName("asda");
        u1.getCountDownList().add(c1);
        u1.getCountDownList().add(c2);
        u1.getCategoryList().add(category1);
        u1.getCategoryList().add(category2);


//        Mapper mapper = new DozerBeanMapper();
//        UserDTO u1DTO = mapper.map(u1, UserDTO.class);

        UserDTO u1DTO = toDTO(u1);

        System.out.println(u1DTO.getUserName());
        for(CategoryDTO categoryDTO  : u1DTO.getCategoryDTOList()){
            System.out.print(categoryDTO.getUser().getUserName());
        }
    }

}
