package com.entity;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Onur Kuru on 11.7.2015.
 */

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userID;

    private String userName;

    @ManyToMany
    private List<CountDown> countDownList = new ArrayList<CountDown>();

    @OneToMany(mappedBy = "user")
    private List<Category> categoryList = new ArrayList<Category>();

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<CountDown> getCountDownList() {
        return countDownList;
    }

    public void setCountDownList(List<CountDown> countDownList) {
        this.countDownList = countDownList;
    }
}
