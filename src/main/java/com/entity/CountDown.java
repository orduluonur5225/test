package com.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Onur Kuru on 11.7.2015.
 */

@Entity
public class CountDown {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int countDownID;

    private String countDownName;

    @ManyToMany
    private List<User> userList = new ArrayList<User>();

    public int getCountDownID() {
        return countDownID;
    }

    public void setCountDownID(int countDownID) {
        this.countDownID = countDownID;
    }

    public String getCountDownName() {
        return countDownName;
    }

    public void setCountDownName(String countDownName) {
        this.countDownName = countDownName;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
